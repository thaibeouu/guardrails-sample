[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Sample project for Guardrails interview

Steps to run this project:

1. Run `docker-compose up`
