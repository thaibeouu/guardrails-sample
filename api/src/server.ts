import 'reflect-metadata';
import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import { createResult, getResults, getResultById, updateResultById, deleteResultById } from './routes/results';

const server = (): express.Express => {
  const app = express();
  app.use(helmet());
  app.use(cors());
  app.use(bodyParser.json());

  app.post('/results', createResult());
  app.get('/results', getResults());
  app.get('/results/:id', getResultById());
  app.put('/results/:id', updateResultById());
  app.delete('/results/:id', deleteResultById());

  return app;
};

export default server;
