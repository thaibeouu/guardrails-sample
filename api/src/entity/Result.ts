import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Finding, Status } from '../types';

@Entity()
export class Result {
  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @Column({
    type: 'enum',
    enum: Status,
    default: Status.QUEUED,
  })
  status: Status;

  @Column('jsonb')
  findings: { findings: Finding[] };

  @Column({
    type: 'varchar',
    default: 'default',
  })
  repositoryName: string;

  @Column({ type: 'timestamp with time zone', nullable: true })
  queuedAt?: Date;

  @Column({ type: 'timestamp with time zone', nullable: true })
  scanningAt?: Date;

  @Column({ type: 'timestamp with time zone', nullable: true })
  finishedAt?: Date;
}
