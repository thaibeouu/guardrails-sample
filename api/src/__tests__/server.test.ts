import server from '../server';

describe('server', () => {
  it('creates express server', () => {
    const e = server();
    expect(e).toBeDefined();
  });
});
