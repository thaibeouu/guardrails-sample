import httpMocks from 'node-mocks-http';
import { createResult, getResults, getResultById, deleteResultById, updateResultById } from '../results';
const next = () => {};

it('GET /results, expect to get 500', async () => {
  const req = httpMocks.createRequest({
    method: 'GET',
    baseUrl: '/results',
  });
  const res = httpMocks.createResponse();
  await getResults()(req, res, next);
  expect(res.statusCode).toEqual(500);
});

it('POST /results, expect to get 500', async () => {
  const req = httpMocks.createRequest({
    method: 'POST',
    baseUrl: '/results',
  });
  const res = httpMocks.createResponse();
  await createResult()(req, res, next);
  expect(res.statusCode).toEqual(500);
});

it('GET /results/:id, expect to get 500', async () => {
  const req = httpMocks.createRequest({
    method: 'GET',
    baseUrl: '/results/1',
  });
  const res = httpMocks.createResponse();
  await getResultById()(req, res, next);
  expect(res.statusCode).toEqual(500);
});

it('PUT /results, expect to get 500', async () => {
  const req = httpMocks.createRequest({
    method: 'PUT',
    baseUrl: '/results/1',
  });
  const res = httpMocks.createResponse();
  await updateResultById()(req, res, next);
  expect(res.statusCode).toEqual(500);
});

it('DELETE /results, expect to get 500', async () => {
  const req = httpMocks.createRequest({
    method: 'DELETE',
    baseUrl: '/results/1',
  });
  const res = httpMocks.createResponse();
  await deleteResultById()(req, res, next);
  expect(res.statusCode).toEqual(500);
});
