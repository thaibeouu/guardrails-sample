import { RequestHandler } from 'express';
import { getRepository } from 'typeorm';
import { Result } from '../entity/Result';

export const createResult = (): RequestHandler => {
  return async (req, res): Promise<void> => {
    if (!req.body) {
      res.status(500).send(new Error('Request body missing'));
    } else {
      try {
        const body: Result = req.body;
        const response = await getRepository(Result).save(body);
        res.status(200).json(response);
      } catch (error) {
        res.status(500).send(error);
      }
    }
  };
};

export const getResults = (): RequestHandler => {
  return async (req, res): Promise<void> => {
    try {
      const response = await getRepository(Result).find({
        order: {
          queuedAt: 'DESC',
          scanningAt: 'DESC',
          finishedAt: 'DESC',
        },
      });
      res.status(200).json(response);
    } catch (error) {
      res.status(500).send(error);
    }
  };
};

export const getResultById = (): RequestHandler => {
  return async (req, res): Promise<void> => {
    try {
      const response = await getRepository(Result).findOne(req.params.id);
      if (response) {
        res.status(200).json(response);
      } else {
        res.sendStatus(404);
      }
    } catch (error) {
      res.status(500).send(error);
    }
  };
};

export const updateResultById = (): RequestHandler => {
  return async (req, res): Promise<void> => {
    try {
      const resultRepo = getRepository(Result);
      const existingResult = await resultRepo.findOne(req.params.id);
      if (existingResult) {
        resultRepo.merge(existingResult, req.body);
        const response = await resultRepo.save(existingResult);
        res.status(200).json(response);
      } else {
        res.sendStatus(404);
      }
    } catch (error) {
      res.status(500).send(error);
    }
  };
};

export const deleteResultById = (): RequestHandler => {
  return async (req, res): Promise<void> => {
    try {
      const response = await getRepository(Result).delete(req.params.id);
      if (response) {
        res.status(200).json(response);
      } else {
        res.sendStatus(404);
      }
    } catch (error) {
      res.status(500).send(error);
    }
  };
};
