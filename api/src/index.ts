import 'reflect-metadata';
import { createConnection } from 'typeorm';
import server from './server';
import { purgeData, insertSampleData } from './utils';

const startServer = () => {
  const port = Number(process.env.PORT) || 3000;
  server().listen(port, () => {
    console.log({ port }, 'Server started');
  });
};

(async () => {
  const connection = await createConnection();
  await purgeData(connection);
  await insertSampleData(connection);
  startServer();
})();

process.on('SIGINT', () => process.exit());
