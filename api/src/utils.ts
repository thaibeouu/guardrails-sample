import 'reflect-metadata';
import { Connection } from 'typeorm';
import { Result } from './entity/Result';
import { Status } from './types';

export const sampleData: Result[] = [
  {
    status: Status.IN_PROGRESS,
    repositoryName: 'react',
    scanningAt: new Date('2020-03-27T04:51:56.038Z'),
    findings: {
      findings: [
        {
          type: 'dast',
          ruleId: 'G7',
          location: {
            path: 'src/index.js',
            positions: {
              begin: {
                line: 10,
              },
            },
          },
          metadata: {
            description: 'Unsafe Router usage.',
            severity: 'Medium',
          },
        },
        {
          type: 'sast',
          ruleId: 'G7',
          location: {
            path: 'src/script.sh',
            positions: {
              begin: {
                line: 10,
              },
            },
          },
          metadata: {
            description: 'The cake is a lie.',
            severity: 'Critical',
          },
        },
      ],
    },
  },
  {
    status: Status.SUCCESS,
    repositoryName: 'linux',
    finishedAt: new Date('2020-03-27T04:51:56.038Z'),
    findings: {
      findings: [
        {
          type: 'sast',
          ruleId: 'G7',
          location: {
            path: 'kernel/async.c',
            positions: {
              begin: {
                line: 10,
              },
            },
          },
          metadata: {
            description: 'What are you doing Linus?',
            severity: 'High',
          },
        },
        {
          type: 'sast',
          ruleId: 'G7',
          location: {
            path: 'kernel/audit.h',
            positions: {
              begin: {
                line: 22,
              },
            },
          },
          metadata: {
            description: 'The cake is a lie.',
            severity: 'Low',
          },
        },
      ],
    },
  },
];

export const insertSampleData = (connection: Connection) => {
  return connection.getRepository(Result).insert(sampleData);
};

export const purgeData = (connection: Connection) => {
  return connection.getRepository(Result).clear();
};
