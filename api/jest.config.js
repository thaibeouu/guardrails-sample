// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  bail: true,
  clearMocks: true,
  collectCoverage: true,
  roots: ['src'],
  preset: 'ts-jest',
  setupFilesAfterEnv: ['jest-extended'],
  testRegex: '/__tests__/((?!int).)*\\.test\\.ts$',
  collectCoverageFrom: [
    '<rootDir>/**/*.{ts,tsx}',
    '!**/jest*config.js',
    '!<rootDir>/src/index.ts',
    '!<rootDir>/**/__tests__/**/*',
  ],
  coverageThreshold: {
    global: {
      statements: 90,
      branches: 78,
      functions: 88,
      lines: 90,
    },
  },
  globals: {
    'ts-jest': {
      diagnostics: {
        ignoreCodes: [
          '6192', // All imports in import declaration are unused.
          '6133', // '{0}' is declared but its value is never read.
        ],
      },
    },
  },
  modulePathIgnorePatterns: [],
  testEnvironment: 'node',
};
