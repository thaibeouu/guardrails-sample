export interface ResultObj {
  status: Status;
  repositoryName: string;
  findings: {
    findings: Finding[];
  };
  id?: string;
  queuedAt?: string;
  scanningAt?: string;
  finishedAt?: string;
}

export interface Finding {
  type: string;
  ruleId: string;
  location: {
    path: string;
    positions: {
      begin: {
        line: number;
      };
    };
  };
  metadata: {
    description: string;
    severity: string;
  };
}

export enum Status {
  QUEUED = 'Queued',
  IN_PROGRESS = 'In Progress',
  SUCCESS = 'Success',
  FAILURE = 'Failure',
}

export interface Action {
  type: string;
  payload: [] | ResultObj | ResultObj[]
}
