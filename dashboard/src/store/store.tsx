import React, { createContext, useContext, useReducer } from 'react';
import { Action } from '../types';

const ctx = createContext([]);
const initialState = { payload: [] };

const reducer = (_: any, action: Action) => {
  switch (action.type) {
    case 'submit':
      return {
        payload: action.payload,
      };
    case 'get':
      return {
        payload: action.payload,
      };
    case 'get_all':
      return {
        payload: action.payload,
      };
    case 'reset':
      return {
        payload: [],
      };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
};

// @ts-ignore
export const StoreProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    // @ts-ignore
    <ctx.Provider value={{ state, dispatch }}>{children}</ctx.Provider>
  );
};

export const useStore = () => useContext(ctx);
