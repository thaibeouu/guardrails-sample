import { useStore } from './store';
import axios, { AxiosResponse } from 'axios';
import { ResultObj } from '../types';

export const useDispatcher = () => {
  const apiHost = process.env.API_HOST || 'http://localhost:3000';
  // @ts-ignore
  const { state, dispatch } = useStore();
  return {
    payload: state ? state.payload : [],
    submitResult: (body: ResultObj) => {
      const submitData = async () => {
        const result: AxiosResponse = await axios.post(`${apiHost}/results`, body);
        dispatch({ type: 'submit', payload: result.data });
      };
      submitData();
    },
    getResults: () => {
      const fetchData = async () => {
        const result: AxiosResponse = await axios(`${apiHost}/results`);
        dispatch({ type: 'get_all', payload: result.data });
      };
      fetchData();
    },
    getResult: (id: string) => {
      const fetchData = async () => {
        const result: AxiosResponse = await axios(`${apiHost}/results/${id}`);
        dispatch({ type: 'get', payload: result.data });
      };
      fetchData();
    },
    reset: () => {
      dispatch({ type: 'reset', payload: [] });
    },
  };
};
