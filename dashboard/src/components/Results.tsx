import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { useDispatcher } from '../store/storeApi';
import { ResultObj, Status } from '../types';

const getColourByStatus = (status: string) => {
  switch (status) {
    case Status.QUEUED:
      return 'indigo';
    case Status.IN_PROGRESS:
      return 'orange';
    case Status.SUCCESS:
      return 'green';
    default:
      return 'red';
  }
};

const getTimestampByStatus = (item: ResultObj) => {
  const dateFromString = (date?: string) => {
    return date ? new Date(date).toLocaleString() : 'a certain point in the future';
  };
  switch (item.status) {
    case Status.QUEUED:
      return `Queued at ${dateFromString(item.queuedAt)}.`;
    case Status.IN_PROGRESS:
      return `Being scanned from ${dateFromString(item.scanningAt)}.`;
    default:
      return `Finished at ${dateFromString(item.finishedAt)}.`;
  }
};

export default function Results() {
  const { payload, getResults } = useDispatcher();
  useEffect(() => {
    getResults();
  }, []);
  return (
    <div className="container mx-auto px-4 sm:px-8">
      <div className="py-8">
        <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
          <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
            <table className="table-auto min-w-full leading-normal">
              <thead>
                <tr>
                  <th className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Repository
                  </th>
                  <th className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Status
                  </th>
                  <th className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Findings
                  </th>
                  <th className="px-3 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Timestamp
                  </th>
                </tr>
              </thead>
              <tbody>
                {payload.length
                  ? payload.map((item: ResultObj) => (
                      <tr key={item.id}>
                        <td className="px-3 py-3 border-b border-gray-200 bg-white text-sm">
                          <div className="flex items-center">
                            <div className="ml-3">
                              <p className="text-gray-900 whitespace-no-wrap">{item.repositoryName}</p>
                            </div>
                          </div>
                        </td>
                        <td className="px-3 py-3 border-b border-gray-200 bg-white text-sm">
                          <span
                            className={`relative inline-block px-3 py-1 text-${getColourByStatus(
                              item.status,
                            )}-900 leading-tight`}
                          >
                            <span
                              aria-hidden
                              className={`absolute inset-0 bg-${getColourByStatus(
                                item.status,
                              )}-300 opacity-50 rounded-full`}
                            ></span>
                            <span className="relative">{item.status}</span>
                          </span>
                        </td>
                        <td className="px-3 py-3 border-b border-gray-200 bg-white text-md">
                          <NavLink
                            to={`results/${item.id}`}
                            className="relative inline-block px-3 py-1 font-semibold text-gray-900 leading-tight"
                          >
                            <span
                              aria-hidden
                              className="absolute inset-0 bg-gray-300 border-2 border-gray-400 rounded-full"
                            ></span>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="mr-1 -mt-1 inline-block opacity-75"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              strokeWidth="2"
                              stroke="currentColor"
                              fill="none"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            >
                              <rect x="0" y="0" width="24" height="24" stroke="none"></rect>
                              <path d="M16 6h3a 1 1 0 011 1v11a2 2 0 0 1 -4 0v-13a1 1 0 0 0 -1 -1h-10a1 1 0 0 0 -1 1v12a3 3 0 0 0 3 3h11"></path>
                              <line x1="8" y1="8" x2="12" y2="8"></line>
                              <line x1="8" y1="12" x2="12" y2="12"></line>
                              <line x1="8" y1="16" x2="12" y2="16"></line>
                            </svg>
                            <span className="relative text-lg text-gray-900 opacity-75">
                              {item.findings.findings.length}
                            </span>
                          </NavLink>
                        </td>
                        <td className="px-3 py-3 border-b border-gray-200 bg-white text-sm">
                          <p className="text-gray-900 whitespace-no-wrap">{getTimestampByStatus(item)}</p>
                        </td>
                      </tr>
                    ))
                  : null}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}
