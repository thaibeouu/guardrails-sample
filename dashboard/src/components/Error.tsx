import React from 'react';

export default function Submit() {
  return (
    <div className="bg-gray-50">
      <div className="max-w-screen-xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between">
        <h2 className="text-3xl leading-9 font-extrabold tracking-tight text-gray-900 sm:text-4xl sm:leading-10">
          Page not found.
          <br />
          <span className="text-blue-600">How did you even come here?</span>
        </h2>
      </div>
    </div>
  );
}
