import React, { Fragment, useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { useDispatcher } from '../store/storeApi';
import { Status, ResultObj } from '../types';

const getTimestampByStatus = (status: Status) => {
  switch (status) {
    case Status.QUEUED:
      return 'queuedAt';
    case Status.IN_PROGRESS:
      return 'scanningAt';
    default:
      return 'finishedAt';
  }
};

export default function Submit() {
  const { payload, submitResult, reset } = useDispatcher();
  const { register, handleSubmit, errors } = useForm<ResultObj>();
  const [sections, setSections] = useState([0]);
  useEffect(() => {
    reset();
  }, []);
  const onAddingField = () => {
    const values = [...sections];
    values.push(values[values.length - 1] + 1);
    setSections(values);
    if (errors.findings) {
      errors.findings.findings!.push({});
    }
  };
  const onRemovingField = (index: number) => {
    const values = [...sections];
    values.splice(index, 1);
    if (errors.findings) {
      errors.findings.findings!.splice(index, 1);
    }
    setSections(values);
  };
  const onSubmit = (data: any) => {
    data[getTimestampByStatus(data.status)] = new Date(data.timestamp);
    submitResult(data);
  };
  return (
    <div className="w-full p-10">
      <form onSubmit={handleSubmit(onSubmit)} className="bg-white shadow-xl rounded px-8 pt-6 pb-8 mb-4">
        <div className="relative flex flex-wrap -mx-3 mb-4">
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label className="block text-gray-700 text-md font-bold mb-2">Repository Name</label>
            <input
              ref={register({ required: true })}
              name="repositoryName"
              className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              type="text"
              placeholder="default"
            />
            {errors.repositoryName && <p className="text-red-500 text-xs italic">Required</p>}
          </div>
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label className="block text-gray-700 text-md font-bold mb-2">Status</label>
            <div className="relative">
              <select
                name="status"
                ref={register}
                className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              >
                {Object.values(Status).map((value) => (
                  <option key={value}>{value}</option>
                ))}
              </select>
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
              </div>
            </div>
          </div>
          <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <label className="block text-gray-700 text-md font-bold mb-2">Timestamp</label>
            <div className="relative">
              <input
                ref={register({ required: true })}
                name="timestamp"
                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                type="datetime-local"
              />
              {errors.hasOwnProperty('timestamp') && <p className="text-red-500 text-xs italic">Required</p>}
            </div>
          </div>
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-md font-bold mb-2">Findings</label>
          {sections.map((section, index) => (
            <Fragment key={`${section}~${index}`}>
              <div className="relative flex flex-wrap -mx-3 mb-2 mt-4">
                <span className="absolute right-0 -mt-2 mr-3 text-xs">
                  {sections.length > 1 && (
                    <button
                      className="bg-red-500 hover:bg-red-700 text-white py-1 px-2 mr-1 rounded focus:outline-none focus:shadow-outline"
                      type="button"
                      onClick={() => onRemovingField(index)}
                    >
                      Remove
                    </button>
                  )}
                  <button
                    className="bg-green-500 hover:bg-green-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline"
                    type="button"
                    onClick={() => onAddingField()}
                  >
                    Add
                  </button>
                </span>
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                  <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">Type</label>
                  <input
                    ref={register({ required: true })}
                    name={`findings.findings[${index}].type`}
                    className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    type="text"
                    placeholder="sast"
                  />
                  {errors.findings && errors.findings.findings!.length > index && errors!.findings!.findings![index].type && (
                    <p className="text-red-500 text-xs italic">Required</p>
                  )}
                </div>
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                  <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">Rule ID</label>
                  <input
                    ref={register({ required: true })}
                    name={`findings.findings[${index}].ruleId`}
                    className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    type="text"
                    placeholder="G402"
                  />
                  {errors.findings && errors.findings.findings!.length > index && errors!.findings!.findings![index].ruleId && (
                    <p className="text-red-500 text-xs italic">Required</p>
                  )}
                </div>
              </div>
              <div className="flex flex-wrap -mx-3 mb-2">
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                  <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">Path</label>
                  <input
                    ref={register({ required: true })}
                    name={`findings.findings[${index}].location.path`}
                    className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    type="text"
                    placeholder="connectors/apigateway.go"
                  />
                  {errors.findings && errors.findings.findings!.length > index && errors!.findings!.findings![index].location?.path && (
                    <p className="text-red-500 text-xs italic">Required</p>
                  )}
                </div>
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                  <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">Line</label>
                  <input
                    ref={register({ required: true, pattern: /^[0-9]*$/ })}
                    name={`findings.findings[${index}].location.positions.begin.line`}
                    className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    type="text"
                    placeholder="60"
                  />
                  {errors.findings && errors.findings.findings!.length > index && errors!.findings!.findings![index].location?.positions && (
                    <p className="text-red-500 text-xs italic">Required</p>
                  )}
                </div>
              </div>
              <div className="flex flex-wrap -mx-3 mb-2 border-b">
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                  <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Description
                  </label>
                  <input
                    ref={register({ required: true })}
                    name={`findings.findings[${index}].metadata.description`}
                    className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    type="text"
                    placeholder="TLS InsecureSkipVerify set true."
                  />
                  {errors.findings && errors.findings.findings!.length > index && errors!.findings!.findings![index].metadata?.description && (
                    <p className="text-red-500 text-xs italic">Required</p>
                  )}
                </div>
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0 pb-5">
                  <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">Severity</label>
                  <div className="relative">
                    <select
                      name={`findings.findings[${index}].metadata.severity`}
                      ref={register({ required: true })}
                      className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    >
                      {['Critical', 'High', 'Medium', 'Low'].map((value) => (
                        <option key={value}>{value}</option>
                      ))}
                    </select>
                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                      <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                      </svg>
                    </div>
                    {errors.findings && errors.findings.findings!.length > index && errors!.findings!.findings![index].metadata?.severity && (
                      <p className="text-red-500 text-xs italic">Required</p>
                    )}
                  </div>
                </div>
              </div>
            </Fragment>
          ))}
        </div>
        <div className="flex items-center justify-between">
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="submit"
          >
            Submit
          </button>
          {payload.id && (
            <span className="text-gray-700">
              Scan posted successfully, please check{' '}
              <Link to="/results" className="text-blue-700 text-underline">
                here!
              </Link>
            </span>
          )}
        </div>
      </form>
    </div>
  );
}
