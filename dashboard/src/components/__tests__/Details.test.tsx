import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';
import Details from '../Details';

test('Details page rendered', () => {
  const { getByText } = render(
    <MemoryRouter>
      <Details />
    </MemoryRouter>,
  );
  const linkElement = getByText(/Findings/i);
  expect(linkElement).toBeInTheDocument();
});
