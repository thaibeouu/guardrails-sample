import React from 'react';
import { render } from '@testing-library/react';
import Results from '../Results';

test('Results page rendered', () => {
  const { getByText } = render(<Results />);
  const linkElement = getByText(/Repository/i);
  expect(linkElement).toBeInTheDocument();
});
