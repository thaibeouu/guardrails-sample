import React from 'react';
import { render } from '@testing-library/react';
import Submit from '../Submit';

test('Submit page rendered', () => {
  const { getByText } = render(<Submit />);
  const linkElement = getByText(/Repository Name/i);
  expect(linkElement).toBeInTheDocument();
});
