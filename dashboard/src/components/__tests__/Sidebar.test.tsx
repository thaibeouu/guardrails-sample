import React from 'react';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Sidebar from '../Sidebar';

test('Sidebar rendered', () => {
  const { getByText } = render(
    <MemoryRouter>
      <Sidebar />
    </MemoryRouter>,
  );
  const linkElement = getByText(/◕/i);
  expect(linkElement).toBeInTheDocument();
});
