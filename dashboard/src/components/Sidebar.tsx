import React from 'react';
import { NavLink } from 'react-router-dom';

export default function Sidebar() {
  return (
    <div className="bg-white w-64 min-h-screen overflow-y-auto hidden md:block shadow relative z-30">
      <div className="flex items-center px-6 py-3 h-16">
        <div className="text-2xl font-bold tracking-tight text-gray-800">(づ｡◕‿‿◕｡)づ</div>
      </div>
      <div className="px-4 py-2">
        <ul>
          <li>
            <NavLink
              to="/submit"
              activeClassName="text-blue-600 bg-gray-200"
              className="mb-1 px-2 py-2 rounded-lg flex items-center font-medium text-gray-700 hover:text-blue-600 hover:bg-gray-200"
            >
              Submit
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/results"
              activeClassName="text-blue-600 bg-gray-200"
              className="mb-1 px-2 py-2 rounded-lg flex items-center font-medium text-gray-700 hover:text-blue-600 hover:bg-gray-200"
            >
              Results
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
}
