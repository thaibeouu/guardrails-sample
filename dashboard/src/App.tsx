import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Detail from './components/Details';
import Results from './components/Results';
import Submit from './components/Submit';
import Sidebar from './components/Sidebar';
import Error from './components/Error';

function App() {
  return (
    <div className="container mx-auto shadow-xl grid grid-cols-5 gap-2 w-screen h-screen bg-gray-200">
      <div className="col-span-1">
        <Sidebar />
      </div>
      <div className="col-span-4 overflow-y-auto">
        <Switch>
          <Redirect exact from="/" to="/submit" />
          <Route path="/submit">
            <Submit />
          </Route>
          <Route path="/results" exact>
            <Results />
          </Route>
          <Route path="/results/:id">
            <Detail />
          </Route>
          <Route>
            <Error />
          </Route>
        </Switch>
      </div>
    </div>
  );
}
export default App;
